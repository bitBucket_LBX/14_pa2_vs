﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _14_BingXiang_VS
{
    public partial class Form1 : Form
    {

        int firstNumber, secondNumber, total;



        private void btn_Quit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnSub_Click_1(object sender, EventArgs e) {
            firstNumber = int.Parse(txt_FirstNumber.Text);
            secondNumber = int.Parse(txt_SecondNumber.Text);

            total = firstNumber - secondNumber;

            MessageBox.Show("The difference between two of the first and second number is : " + total.ToString());
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void btnMulti_Click(object sender, EventArgs e) {
            firstNumber = int.Parse(txt_FirstNumber.Text);
            secondNumber = int.Parse(txt_SecondNumber.Text);

            total = firstNumber * secondNumber;

            MessageBox.Show("The product of the first and second number is : " + total.ToString());
        }

        private void btnDiv_Click(object sender, EventArgs e) {
            firstNumber = int.Parse(txt_FirstNumber.Text);
            secondNumber = int.Parse(txt_SecondNumber.Text);

            total = firstNumber / secondNumber;

            MessageBox.Show("The division between two of the first and second number is : " + total.ToString());
        }

        private void btn_Add_Click(object sender, EventArgs e)
        {
             firstNumber = int.Parse(txt_FirstNumber.Text);
             secondNumber = int.Parse(txt_SecondNumber.Text);

             total = firstNumber + secondNumber;

            MessageBox.Show("The sum of the first and second number is : "+total.ToString());

        }

    }
}
